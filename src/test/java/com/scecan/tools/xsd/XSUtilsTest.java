package com.scecan.tools.xsd;

import com.sun.xml.xsom.XSElementDecl;
import com.sun.xml.xsom.XSSchema;
import com.sun.xml.xsom.XSSchemaSet;
import com.sun.xml.xsom.parser.XSOMParser;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.xml.sax.SAXException;

/**
 * @author Sandu Cecan (scecan@gmail.com)
 */
public class XSUtilsTest {
    
    private static final String A_XSD = "A.xsd";
    private static final String A_TARGET_NAMESPACE = "http://xml.scecan.com/schema/A";
    private static final String B_XSD = "B.xsd";
    private static final String B_TARGET_NAMESPACE = "http://xml.scecan.com/schema/B";


    private XSSchema schemaA;
    private XSSchema schemaB;

    public XSUtilsTest() throws SAXException {
        XSOMParser parserA = new XSOMParser();
        parserA.parse(ClassLoader.getSystemResource(A_XSD));
        XSSchemaSet schemaSetA = parserA.getResult();
        schemaA = schemaSetA.getSchema(A_TARGET_NAMESPACE);

        XSOMParser parserB = new XSOMParser();
        parserB.parse(ClassLoader.getSystemResource(B_XSD));
        XSSchemaSet schemaSetB = parserB.getResult();
        schemaB = schemaSetB.getSchema(B_TARGET_NAMESPACE);
    }

    @Test
    public void testFindSubElement() {
        // simple schema
        subElementExists("Level1A", schemaA, "tRoot", A_TARGET_NAMESPACE);
        subElementExists("Level2A", schemaA, "tLevel1", A_TARGET_NAMESPACE);
        subElementExists("Level3A", schemaA, "tLevel2", A_TARGET_NAMESPACE);
        subElementDoesntExist("NonExistent", schemaA, "tLevel1", A_TARGET_NAMESPACE);
        // combined schema
        subElementExists("Level1A", schemaB, "tRoot", A_TARGET_NAMESPACE);
        subElementExists("Level1B", schemaB, "tRoot", B_TARGET_NAMESPACE);
        subElementExists("Level2A", schemaB, "tLevel1", A_TARGET_NAMESPACE);
        subElementExists("Level2B", schemaB, "tLevel1", B_TARGET_NAMESPACE);
        subElementExists("Level3A", schemaB, "tLevel2", A_TARGET_NAMESPACE);
        subElementExists("Level3B", schemaB, "tLevel2", B_TARGET_NAMESPACE);
        subElementDoesntExist("NonExistent", schemaA, "tLevel1", A_TARGET_NAMESPACE);
        subElementDoesntExist("NonExistent", schemaA, "tLevel1", B_TARGET_NAMESPACE);
    }

    private void subElementExists(String subElementName, XSSchema schema, String typeName, String targetNamespace) {
        assertNotNull(String.format("'%s' schema doesn't have '%s' type defined", schema.getTargetNamespace(), typeName),
                schema.getType(typeName));
        XSElementDecl level1El = XSUtils.findSubElement(subElementName, schema.getType(typeName));
        assertNotNull(String.format("Elements of '%s' type in '%s' namespace should be able to contain '%s' subelement",
                typeName, targetNamespace, subElementName), level1El);
        assertEquals(subElementName, level1El.getName());
        assertEquals(targetNamespace, level1El.getTargetNamespace());
    }

    private void subElementDoesntExist(String subElementName, XSSchema schema, String typeName, String targetNamespace) {
        assertNotNull(String.format("'%s' schema doesn't have '%s' type defined", schema.getTargetNamespace(), typeName),
                schema.getType(typeName));
        XSElementDecl level1El = XSUtils.findSubElement(subElementName, schema.getType(typeName));
        assertNull(String.format("Elements of '%s' type in '%s' namespace shouldn't be able to contain '%s' subelement",
                typeName, targetNamespace, subElementName), level1El);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindSubElementsIAE() throws Exception {
        XSUtils.findSubElement(null, null);
    }

    @Test
    public void testValidateElementsPath() throws PathException {
        //simple schema
        String pathA1 = "RootA/Level1A";
        XSUtils.validateElementsPath(pathA1.split("/"), schemaA);
        String pathA2 = "RootA/Level1A/Level2A/Level3A";
        XSUtils.validateElementsPath(pathA2.split("/"), schemaA);
        //combined schema
        String pathB1 = "RootB/Level1A";
        XSUtils.validateElementsPath(pathB1.split("/"), schemaB);
        String pathB2 = "RootB/Level1B";
        XSUtils.validateElementsPath(pathB2.split("/"), schemaB);
        String pathB3 = "RootB/Level1B/Level2A";
        XSUtils.validateElementsPath(pathB3.split("/"), schemaB);
        String pathB4 = "RootB/Level1B/Level2B/Level3A";
        XSUtils.validateElementsPath(pathB4.split("/"), schemaB);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateElementsPathIAE() throws PathException {
        XSUtils.validateElementsPath(null, null);
    }

    @Test(expected = PathException.class)
    public void testValidateElementsPathException() throws PathException {
        String pathA1 = "RootA/NonExistent";
        XSUtils.validateElementsPath(pathA1.split("/"), schemaA);
    }

    @Test(expected = PathException.class)
    public void testValidateElementsPathExceptionRoot() throws PathException {
        String pathA1 = "NonExistentRoot";
        XSUtils.validateElementsPath(pathA1.split("/"), schemaA);
    }


}
