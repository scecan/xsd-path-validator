package com.scecan.tools.xsd;

/**
 * @author Sandu Cecan (scecan@gmail.com)
 */
public class PathException extends Exception {
    public PathException() {
        super();
    }

    public PathException(String message) {
        super(message);
    }

    public PathException(String message, Throwable cause) {
        super(message, cause);
    }

    public PathException(Throwable cause) {
        super(cause);
    }
}
