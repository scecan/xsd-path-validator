package com.scecan.tools.xsd;

import com.sun.xml.xsom.*;

/**
 * Some util methods to analyze a schema file.
 *
 * @author Sandu Cecan (scecan@gmail.com)
 */
public final class XSUtils {

    private XSUtils() {}

    /**
     * This method search for the element with given name for given type and returns it.
     * @param localName name of the element to find
     * @param xsType the type where to search
     * @return the found element or null if it does not exist
     */
    public static XSElementDecl findSubElement(String localName, XSType xsType) {
        if (localName == null || xsType == null) {
            throw new IllegalArgumentException("localName and xsType arguments should not be null");
        }
        XSElementDecl xsElement = null;
        if (xsType.isComplexType()) {
            XSParticle xsParticle = xsType.asComplexType().getContentType().asParticle();
            if (xsParticle != null) {
                XSTerm xsTerm = xsParticle.getTerm();
                if (xsTerm.isModelGroup()) {
                    XSModelGroup xsModelGroup = xsTerm.asModelGroup();
                    xsElement = findSubElement(localName, xsModelGroup);
                }
            }
        }
        if (xsElement == null) {
            if (xsType != xsType.getBaseType()) { //avoid infinite loops
                xsElement = findSubElement(localName, xsType.getBaseType());
            }
        }
        return xsElement;
    }

    private static XSElementDecl findSubElement(String localName, XSModelGroup xsModelGroup) {
        for (XSParticle particle : xsModelGroup.getChildren()) {
            XSTerm term = particle.getTerm();
            if (term.isModelGroup()) {
                XSElementDecl xse = findSubElement(localName, term.asModelGroup());
                if (xse != null) {
                    return xse;
                }
            } else if (term.isElementDecl()) {
                XSElementDecl xse = term.asElementDecl();
                if (localName.equals(xse.getName())) {
                    return xse;
                }
            }
        }
        return null;
    }

    /**
     * This method checks the validity of the given path for given schema and throws an exception if it is not valid.
     * @param elementNames an array with elements names representing a path
     * @param schema the schema where to check for the path validity
     * @throws PathException if the path is not valid for given schema
     */
    public static void validateElementsPath(String[] elementNames, XSSchema schema) throws PathException {
        if (elementNames == null || schema == null || elementNames.length == 0) {
            throw new IllegalArgumentException("elementNames and schema should not be null or empty");
        }
        String rootName = elementNames[0];
        XSElementDecl root = schema.getElementDecl(rootName);
        if (root == null) {
            throw new PathException(String.format("The '%s' schema doesn't have a '%s' element declaration.",
                    schema.getTargetNamespace(), elementNames[0]));
        }
        XSElementDecl xsElement = root;
        for (int i = 1; i < elementNames.length; i++) {
            XSElementDecl e = findSubElement(elementNames[i], xsElement.getType());
            if (e == null) {
                throw new PathException(String.format("Element '%s' which is of type '%s' cannot cannot contain a '%s' sub element.",
                        xsElement.getName(), xsElement.getType().getName(), elementNames[i]));
            }
            xsElement = e;
        }
    }

}
